#ifndef OS_CORE_H
#define OS_CORE_H

#include <stdint.h>
#include "board.h"

#define STACK_SIZE 256 // in bytes
#define MAX_TASK_COUNT (8 + 1) // 8 user defined tasks plus the idle task

/* Registers */
#define XPSR			1
#define PC_REG			2
#define LR				3
#define R12				4
#define R3				5
#define R2				6
#define R1				7
#define R0				8
#define LR_PREV_VALUE	9
#define R4				10
#define R5				11
#define R6				12
#define R7				13
#define R8				14
#define R9				15
#define R10 			16
#define R11 			17

#define STACK_FRAME_SIZE 17

/************************************************************************************
 * 			Valores necesarios para registros del stack frame inicial
 ***********************************************************************************/
#define INIT_XPSR 	1 << 24				//xPSR.T = 1
#define EXEC_RETURN	0xFFFFFFF9			//retornar a modo thread con MSP, FPU no utilizada

void os_core_set_task(void * entry_point, uint8_t prio);
void os_core_run(void);

#endif // OS_API_H