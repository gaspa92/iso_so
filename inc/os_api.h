#ifndef OS_API_H
#define OS_API_H

#include <stdint.h>

void os_init(void);
void os_create_task(void * entry_point, uint8_t prio);

#endif // OS_API_H