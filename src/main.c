#include <stdlib.h>
#include "board.h"

#include "os_api.h"

#define TASK_A_PRIO 0
#define TASK_B_PRIO 1
#define TASK_C_PRIO 2

void task_A_func(void);
void task_B_func(void);
void task_C_func(void);

int main(void)
{
    Board_Init();
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock / 1000);

    // uartConfig( UART_USB, 115200 );

    os_create_task(task_A_func, TASK_A_PRIO);
    os_create_task(task_B_func, TASK_B_PRIO);
    os_create_task(task_C_func, TASK_C_PRIO);

    os_init();

    while(1){}

    return 0;
}

int a,b,c = 0;

void task_A_func(void)
{
    a++;
}

void task_B_func(void)
{
    b++;
}

void task_C_func(void)
{
    c++;
}

