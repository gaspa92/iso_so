#include "os_core.h"

#include <stdint.h>

#define IDLE_TASK_ID 0

#define MAX_PRIO        0
#define MIN_PRIO        3
#define PRIO_COUNT  ((MIN_PRIO - MAX_PRIO) + 1)

typedef enum {
	TASK_STATE_READY,
	TASK_STATE_RUNNING,
	TASK_STATE_BLOCKED
} os_task_state_t;

typedef struct {
    uint32_t stack[STACK_SIZE / 4];
	uint32_t stack_pointer;
	void * entry_point;
	os_task_state_t state;
	uint8_t prio;
	uint32_t remaining_ticks;
} task_t;

typedef enum {
    SYS_STATE_FROM_RESET,
    SYS_STATE_NORMAL_RUN,
    SYS_STATE_SCHEDULING
} system_state_t;

typedef struct
{
    uint8_t tasks_count;
    system_state_t state;
    task_t tasks[MAX_TASK_COUNT];
    task_t * running_task;
    task_t * next_task;
    uint8_t tasks_per_prio[PRIO_COUNT];
    bool context_switch;
} system_t;

system_t system =
{
    .tasks_count = 1, // Start at 1 because of the idle task
    .state = SYS_STATE_FROM_RESET
};

static void set_pend_sv(void);
static void scheduler(void);
static void init_idle_task(void);
static int32_t partition(task_t * tasks[], int32_t l, int32_t h);
static void order_tasks_by_priority(void);

void __attribute__((weak)) return_hook(void)  {
	while(1);
}

void __attribute__((weak)) tick_hook(void)  {
	__asm volatile( "nop" );
}

void __attribute__((weak)) idle_task(void)  {
	while(1)  {
		__WFI();
	}
}

void os_core_set_task(void * entry_point, uint8_t prio)
{
    uint8_t id;

    if(system.tasks_count > MAX_TASK_COUNT)
    {
        return;
    }

    id = system.tasks_count;

    /* Set thumb bit, task entry point and return hook */
    system.tasks[id].stack[STACK_SIZE/4 - XPSR] = INIT_XPSR;
    system.tasks[id].stack[STACK_SIZE/4 - PC_REG] = (uint32_t)entry_point;
    system.tasks[id].stack[STACK_SIZE/4 - LR] = (uint32_t)return_hook;

    /* Save LR previous value */
    system.tasks[id].stack[STACK_SIZE/4 - LR_PREV_VALUE] = EXEC_RETURN;

    system.tasks[id].stack_pointer = (uint32_t) (system.tasks[id].stack + STACK_SIZE/4 - STACK_FRAME_SIZE);

    system.tasks[id].entry_point = entry_point;
    system.tasks[id].state = TASK_STATE_READY;
    system.tasks[id].prio = prio;
    system.tasks[id].remaining_ticks = 0;

    /* Update system structure */
    system.tasks_per_prio[prio]++;
    system.tasks_count++;
}

void os_core_run(void)
{
	NVIC_SetPriority(PendSV_IRQn, (1 << __NVIC_PRIO_BITS)-1);

	init_idle_task();

	system.state = SYS_STATE_FROM_RESET;
	system.running_task = NULL;
	system.next_task = NULL;

	order_tasks_by_priority();
}

uint32_t get_next_context(uint32_t running_task_sp)
{
	uint32_t next_task_sp;

	system.running_task->stack_pointer = running_task_sp;

	if (system.running_task->state == TASK_STATE_RUNNING)
    {
		system.running_task->state = TASK_STATE_READY;
    }

	next_task_sp = system.next_task->stack_pointer;

	system.running_task = system.next_task;
	system.running_task->state = TASK_STATE_RUNNING;

	system.state = SYS_STATE_NORMAL_RUN;

	return next_task_sp;
}

void SysTick_Handler(void)
{
	scheduler();

	tick_hook();
}

static void init_idle_task(void)
{
	system.tasks[IDLE_TASK_ID].stack[STACK_SIZE/4 - XPSR] = INIT_XPSR;
	system.tasks[IDLE_TASK_ID].stack[STACK_SIZE/4 - PC_REG] = (uint32_t)idle_task;
	system.tasks[IDLE_TASK_ID].stack[STACK_SIZE/4 - LR] = (uint32_t)return_hook;


	system.tasks[IDLE_TASK_ID].stack[STACK_SIZE/4 - LR_PREV_VALUE] = EXEC_RETURN;
	system.tasks[IDLE_TASK_ID].stack_pointer = (uint32_t) (system.tasks[IDLE_TASK_ID].stack + STACK_SIZE/4 - STACK_FRAME_SIZE);


	system.tasks[IDLE_TASK_ID].entry_point = idle_task;
	system.tasks[IDLE_TASK_ID].state = TASK_STATE_READY;
	system.tasks[IDLE_TASK_ID].prio = 0xFF;
}

static void set_pend_sv(void)
{
	/* Reset context switch flag */
	system.context_switch = false;

	SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;
	__ISB();
	__DSB();
}

static void scheduler(void)
{
	static uint8_t task_index_by_prio[PRIO_COUNT];
	uint8_t processing_task_index = 0;
	uint8_t processing_prio = MAX_PRIO;
	bool exit_loop = false;

    /* Set initial conditions from reset */
	if (system.state == SYS_STATE_FROM_RESET)
    {
        system.running_task = &system.tasks[IDLE_TASK_ID];
        memset(task_index_by_prio, 0, sizeof(uint8_t) * PRIO_COUNT);
        system.state = SYS_STATE_NORMAL_RUN;
	}

	if (system.state == SYS_STATE_SCHEDULING)
    {
		return;
	}
	system.state = SYS_STATE_SCHEDULING;

    /* Loop through all tasks until we find a task ready to run */
	while(!exit_loop)  {

		processing_task_index = 0;

		/*
		 * Puede darse el caso de que se hayan definido tareas de prioridades no contiguas, por
		 * ejemplo dos tareas de maxima prioridad y una de minima prioridad. Quiere decir que no
		 * existen tareas con prioridades entre estas dos. Este bloque if determina si existen
		 * funciones para la prioridad actual. Si no existen, se pasa a la prioridad menor
		 * siguiente
		 */
		if(system.tasks_per_prio[processing_prio] > 0)
        {

            /*
            * esta linea asegura que el indice siempre este dentro de los limites de la subseccion
            * que forman los punteros a las tareas de prioridad actual
            */
            task_index_by_prio[processing_prio] %= system.tasks_per_prio[processing_prio];

            /*
            * El bucle for hace una conversion de indices de subsecciones al indice real que debe
            * usarse sobre el vector de punteros a tareas. Cuando se baja de prioridad debe sumarse
            * el total de tareas que existen en la subseccion anterior. Recordar que el vector de
            * tareas esta ordenado de mayor a menor
            */
            for (int i = 0; i < processing_prio; i++)
            {
                processing_task_index += system.tasks_per_prio[i];
            }
            processing_task_index += task_index_by_prio[processing_prio];

            switch(system.tasks[processing_task_index].state)
            {
                case TASK_STATE_READY:
                    system.next_task = &system.tasks[processing_task_index];
                    system.context_switch = true;
                    task_index_by_prio[processing_prio]++;
                    exit_loop = true;
                    break;

                case TASK_STATE_BLOCKED:
                    // TODO: implement blocked state
                    break;

                case TASK_STATE_RUNNING:
                    task_index_by_prio[processing_prio]++;
                    system.context_switch = false;
                    exit_loop = true;
                    break;
            }
        }
        else
        {
            task_index_by_prio[processing_prio] = 0;
            processing_prio++;
        }
	}

	system.state = SYS_STATE_NORMAL_RUN;

	if(system.context_switch)
    {
		set_pend_sv();
    }
}

static void order_tasks_by_priority(void)
{
	// Create an auxiliary stack
	int32_t stack[MAX_TASK_COUNT];

	// initialize top of stack
	int32_t top = -1;
	int32_t l = 0;
	int32_t h = system.tasks_count - 1;

	// push initial values of l and h to stack (indices a estructuras de tareas)
	stack[++top] = l;
	stack[++top] = h;

	// Keep popping from stack while is not empty
	while (top >= 0) {
		// Pop h and l
		h = stack[top--];
		l = stack[top--];

		// Set pivot element at its correct position
		// in sorted array
		int32_t p = partition((task_t **)&system.tasks, l, h);

		// If there are elements on left side of pivot,
		// then push left side to stack
		if (p - 1 > l) {
			stack[++top] = l;
			stack[++top] = p - 1;
		}

		// If there are elements on right side of pivot,
		// then push right side to stack
		if (p + 1 < h) {
			stack[++top] = p + 1;
			stack[++top] = h;
		}
	}
}

static int32_t partition(task_t * tasks[], int32_t l, int32_t h)
{
	task_t * x = tasks[h];
	task_t * aux;
	int32_t i = (l - 1);

	for (int j = l; j <= h - 1; j++)
    {
		if (tasks[j]->prio <= x->prio)
        {
			i++;
			//swap(&tasks[i], &tasks[j]);
			aux = tasks[i];
			tasks[i] = tasks[j];
			tasks[j] = aux;
		}
	}
	//swap(&tasks[i + 1], &tasks[h]);
	aux = tasks[i+1];
	tasks[i+1] = tasks[h];
	tasks[h] = aux;

	return (i + 1);
}