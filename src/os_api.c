#include "os_core.h"

void os_init(void)
{
	os_core_run();
}

void os_create_task(void * entry_point, uint8_t prio)
{
	os_core_set_task(entry_point, prio);
}

